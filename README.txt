DESCRIPTION
------------

This module gives you pre-moderation function of some comments that includes
bad words or disallowed urls. All other comments will be published as usual.
 
INSTALLATION
------------

Standard module installation applies. English bad words and current site url 
will be saved in configuration of module.

REQUIREMENTS
------------

Enabled comment module.

CONFIGURATION
-------------

Configuration page at url: admin/config/content/comment_control
You can edit list of allowed urls, disallowed words and bad words. The last list 
is separated for special purpose - not to hurt your eyes each time you edit
configuration page :-)